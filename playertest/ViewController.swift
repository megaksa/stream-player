//
//  ViewController.swift
//  playertest
//  Few code snippets taken here: https://github.com/jaredsinclair/sodes-audio-example
//  ResourceLoaderDelegate: https://github.com/jaredsinclair/sodes-audio-example/blob/master/Sodes/SodesAudio/ResourceLoaderDelegate.swift
//
//  Created by Sergei Kuzmin on 9/24/18.

import UIKit
import AVKit
import AVFoundation
import CryptoSwift

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  static let chunkBlocksCount = 4096
  static let blockSize = 16
  static let blockMask = ~(blockSize - 1)
  static let chunkSize = 4096*16 // chunkBlocksCount * blockSize

  let queue = DispatchQueue(label: "com.gramophone")
  // encrypted file total bytes downloaded
//  var lastReportedSize: Int64 = 0
//  var downloadedSize:Int64 = 0
  // totalSize we got while downloading encrypted file
//  var totalSize:Int64 = 0
  var requestedLength: Int = 0
  static let iv:[UInt8] = Array(repeating: 0x00, count: 16)
  var key: [UInt8]?

  // Holds the name of file being downloaded in temp directory.
  var inProgressFileUrl:URL?

  @IBAction func playVideo(_ sender: UIButton) {
    print("Touch up")
//        playEncryptedVideo(stringUrl: "http://www.largesound.com/ashborytour/sound/AshboryBYU.mp3")
    playEncryptedVideo(stringUrl: "http://develop.gramophone.by/files/media/album/5b40769b66ff1f58e666635c/5b87ac8ab432a32eba8ed2e8.gr")
  }
  
  func playEncryptedVideo(stringUrl: String) {
    let keyString = "VsQ8nUwp" + stringUrl.suffix(11).prefix(8).reversed()
    self.key = keyString.utf8.map{UInt8($0)} // bytes

    let newUrl = URL(string: "enc" + stringUrl)!
    let asset = AVURLAsset(url: newUrl)
    asset.resourceLoader.setDelegate(self, queue: queue)
    let playerItem = AVPlayerItem(asset: asset)
    // playerItem.preferredForwardBufferDuration = 15
    let player = AVPlayer(playerItem: playerItem)
    player.automaticallyWaitsToMinimizeStalling = false
    
    let controller = AVPlayerViewController()
    controller.player = player
    
    present(controller, animated: true) {
      player.play()
      print("Player must be playing")
    }
  }
}

internal extension URL {
  /// Removes the scheme prefix from a copy of the receiver.
  func convertFromRedirectURL(prefix: String) -> URL? {
    guard var comps = URLComponents(url: self, resolvingAgainstBaseURL: false) else {return nil}
    guard let scheme = comps.scheme else {return nil}
    comps.scheme = scheme.replacingOccurrences(of: prefix, with: "")
    return comps.url
  }
}

// PlayerViewController knows how to load encrypted resources like 'enchttp://somesite.com/somefile.gr'
extension ViewController: AVAssetResourceLoaderDelegate {
  //AVAssetResourceLoaderDelegate loading request.
  func resourceLoader(_ resourceLoader: AVAssetResourceLoader, shouldWaitForLoadingOfRequestedResource loadingRequest: AVAssetResourceLoadingRequest) -> Bool {

    var request = URLRequest(url: loadingRequest.request.url!.convertFromRedirectURL(prefix: "enc")!)
    if let _ = loadingRequest.contentInformationRequest {
      print("contentRequest")
    } else if let _ = loadingRequest.dataRequest {
      print("dataRequest")
    } else {
      print("Unknown request type")
      return false
    }
    let dataDelegate = GPDataDelegate(loadingRequest: loadingRequest, urlRequest: &request, key: key!)
    let session = URLSession(configuration: URLSessionConfiguration.default, delegate: dataDelegate, delegateQueue: OperationQueue.main)
    let dataTask = session.dataTask(with: request)
    dataTask.resume()
    return true
  }
  
  func resourceLoader(_ resourceLoader: AVAssetResourceLoader, shouldWaitForRenewalOfRequestedResource renewalRequest: AVAssetResourceRenewalRequest) -> Bool {
    fatalError()
  }
  
  func resourceLoader(_ resourceLoader: AVAssetResourceLoader, didCancel loadingRequest: AVAssetResourceLoadingRequest) {
    print("cancel")
  }
}

class GPDataDelegate: NSObject, URLSessionDataDelegate {
  
  let loadingRequest: AVAssetResourceLoadingRequest
  // how many bytes to skip when decoded. This is to extend lower bound and allow first block to decode
  var needToSkipCount: Int = 0
  var lowerBound: Int = 0
  var upperBound: Int = 0
  var leftBytes: [UInt8] = []
  let key: [UInt8]
  
  init(loadingRequest: AVAssetResourceLoadingRequest, urlRequest: inout URLRequest, key: [UInt8]) {
    self.loadingRequest = loadingRequest
    self.key = key
    // dataRequest is required
    if let dataRequest = loadingRequest.dataRequest {
      needToSkipCount = Int(dataRequest.requestedOffset) % ViewController.blockSize
      lowerBound = Int(dataRequest.requestedOffset) - needToSkipCount
      upperBound = Int(dataRequest.requestedOffset) + dataRequest.requestedLength
      let adjustedRange = lowerBound..<upperBound
      print("Requesting range: \(dataRequest.byteRange), adjustedRange: \(adjustedRange)")
      // TODO inout param is not great. Think of better interface
      urlRequest.setByteRangeHeader(for: dataRequest.byteRange)
    }
  }
  
  public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
    print("Received chunk: \(lowerBound + leftBytes.count)..<\(lowerBound + leftBytes.count + data.count), total left: \(upperBound - (lowerBound+leftBytes.count))")
    guard !loadingRequest.isCancelled else {
      print("Bailing early because the loading request was cancelled.")
      return
    }

    if let response = dataTask.response as? HTTPURLResponse {
      guard let rangeString = response.allHeaderFields["Content-Range"] as? String,
        let lengthString = rangeString.split(separator: "/").last,
        let contentLength = Int64(lengthString) else {
          print("No content range header")
          loadingRequest.finishLoading(with: NSError(domain: "com.gramophone", code: 1, userInfo: ["message" : "Content-Range header not present"]))
          return
      }
      if let contentInfoRequest = loadingRequest.contentInformationRequest {
        // responding to loading request
        contentInfoRequest.isByteRangeAccessSupported = true
        contentInfoRequest.contentType = AVFileType.mp3.rawValue // "public.mp3"
        contentInfoRequest.contentLength = contentLength
        loadingRequest.finishLoading()
      } else if let dataRequest = loadingRequest.dataRequest {
        let counter = lowerBound / ViewController.blockSize
        var newLowerBound = lowerBound + leftBytes.count + data.count
        // if this is the last chunk, then give the whole sequence for decoding
        let encodedData: [UInt8]
        if newLowerBound == upperBound {
          encodedData = leftBytes + data
          leftBytes = []
        } else {
          // Offset within data.
          let newLeftBytesOffset = (newLowerBound & ViewController.blockMask) - leftBytes.count - lowerBound
          encodedData = leftBytes + data[0..<newLeftBytesOffset]
          leftBytes = Array(data.dropFirst(newLeftBytesOffset))
          newLowerBound -= leftBytes.count
        }
        if encodedData.count > 0 {
          let aes = try! AES(key: key, blockMode: CTR(iv: ViewController.iv, counter: counter), padding: .noPadding)
          let decodedChunk = try! aes.decrypt(encodedData)
          let decodedData: Data
          if needToSkipCount > 0 {
            let skip = min(needToSkipCount, decodedChunk.count)
            decodedData = Data(decodedChunk.dropFirst(skip))
            lowerBound += skip
            needToSkipCount -= skip
          } else {
            decodedData = Data(decodedChunk)
          }
          print("Decoded chunk: \(lowerBound)..<\(newLowerBound). Counter: \(counter). Size: \(encodedData.count), \(decodedData.count)\n    Encoded: \(encodedData[..<128].hexBytes())\n    Decoded: \(decodedData[..<128].hexBytes())")
          dataRequest.respond(with: decodedData)
        }
        lowerBound = newLowerBound
        if lowerBound == upperBound {
          print("Finish loading")
          loadingRequest.finishLoading()
        }
      }
    }
    else {
      print("Error: No associated response")
      loadingRequest.finishLoading()
    }
  }
}

public typealias ByteRange = Range<Int64>

extension AVAssetResourceLoadingDataRequest {
  var byteRange: ByteRange {
    let lowerBound = requestedOffset
    let upperBound = lowerBound + Int64(requestedLength)
    return (lowerBound..<upperBound)
  }
}

extension URLRequest {
  /// Convenience method
  mutating func setByteRangeHeader(for range: ByteRange) {
    let rangeHeader = "bytes=\(range.lowerBound)-\(range.upperBound - 1)"
    setValue(rangeHeader, forHTTPHeaderField: "Range")
  }
}

extension Sequence where Self.Element: CVarArg {
  // Print bytes as space separated hex values
  func hexBytes() -> String {
    return map{String(format:"%02X", $0)}.joined(separator: " ")
  }
}

//extension Data {
//  func chunked(by chunkSize: Int, forRange range:CountableRange<Int>) -> [ArraySlice<UInt8>] {
//    return stride(from: range.lowerBound, to: range.upperBound, by: chunkSize).map {
//      ArraySlice(self[$0..<Swift.min($0 + chunkSize, count)])
//    }
//  }
//}
